class Solution:
    def maxArea(self, height: List[int]) -> int:
        # Find start and end so that the sum is maximum
        # The problem here is the amount of water is depending on:
        #     + Height of column * length of area
        # Either of start point or end point should be a peak

        # [1,7,2,3,5] --> the area is the min(start, end) * number of column
        # Consider the pair: (1,7)*1 -> 1
        # (1,7,3) -> (1)*2 -> 2
        # (7,2,3) -> (3)*2 -> 6
        # For the example:
        #     [1,8,6,2,5,4,8,3,7] -> (1, 7) -> 1*8 = 8
        #      0 1 2 3 4 5 6 7 8  -> (8, 7) -> 7*7 = 49

        # We has the algorithm to calculate the area of water: min(start, end) * (end_index - start_index)
        #   Space Complexity: O(n^2) because we have to go through all of the item
        #.    Because you need one pointer for the start and one for the end

        # Enhance version: Two pointers but not in the same direction (start_pointer, and end_pointer) --> Start++ , end --
        # This one we can make an O(n)

        ## DO IT YOUR SELF, UPDATE LATER ##