class Solution:
    def lastStoneWeight(self, stones: List[int]) -> int:
        # sort first
        stones.sort(reverse=True)
        while(len(stones) > 1):
            # compare first and second
            if stones[0] == stones[1]:
                del(stones[0:2])
            else:
                # stones[0] > stones[1]
                new_weight = stones[0] - stones[1]
                del(stones[0:2])
                if not stones:
                    return new_weight
                if new_weight < stones[-1]:
                    stones.append(new_weight)
                    continue
                for index, value in enumerate(stones):
                    if new_weight >= value:
                        stones.insert(index, new_weight)
                        break
        if stones:
            return stones[0]
        return 0