class Solution:
    def lengthOfLongestSubstring(self, s: str) -> int:
        # We need the following memories:
        # One for holding the longest string length (max)
        # One hashmap to store the character and its index
        # One pointer to move to the position of the previous matched char
        ss_count = 0
        hmap = {}
        start_index = 0  # Start of the substring
        for index, char in enumerate(s):
            if char in hmap.keys():
                # reset the start index to the matched position
                start_index = max(hmap[char] + 1, start_index)

            ss_count = max(ss_count, index - start_index + 1)
            # store char-index to hashmap
            hmap[char] = index
        return ss_count
